package com.example.celsiustofahrenheittranslator;

import java.io.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "helloServlet", value = "/hello-servlet")
public class HelloServlet extends HttpServlet {
    private String message;


    public void init() {
        message = "Type your Celsius temperature: ";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int number = Integer.parseInt((request.getParameter("CelsiusDegree")));
        int Fdegree = number*9/5 -32;
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Celsius to Fahrenheit Traslator";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Celsius temperature: " +
                request.getParameter("CelsiusDegree") + "\n" +
                "  <P>Fahrenheit degree: " + Fdegree +
                "</BODY></HTML>");
    }

}